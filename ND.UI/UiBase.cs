﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ND.UI
{
    public class UiBase
    {
        protected static DataTable SortData(DataTable dataTable, ND.Data.DataObject.SortType sortType, String field)
        {
            DataTable result = new DataTable();
            try
            {
                String str = String.Empty;
                switch (sortType)
                {
                    case ND.Data.DataObject.SortType.Ascending:
                        {
                            str = field + " ASC";
                            break;
                        }
                    case ND.Data.DataObject.SortType.Descending:
                        {
                            str = field + " DESC";
                            break;
                        }
                    case ND.Data.DataObject.SortType.None:
                        {
                            str = String.Empty;
                            break;
                        }
                }
                DataRow[] dataRows = dataTable.Select("", str);
                DataTable newDataTable = new DataTable();
                newDataTable = dataTable.Clone();
                foreach (DataRow newDataRow in dataRows) newDataTable.ImportRow(newDataRow);
                result = newDataTable.Copy();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }
    }
}
