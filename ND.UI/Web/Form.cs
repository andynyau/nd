﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;

namespace ND.UI.Web
{
    public class Form : UiBase
    {
        #region Declaration

        public delegate Boolean DelegateLoadDataMethod(ND.Data.DataObject.SortType sortType, String sortField);

        #endregion

        #region DropDownList

        public static Boolean PopulateDropDownList(ref DropDownList dropDownList, DataTable dataTable, String textField, String valueField)
        {
            return Form.PopulateDropDownList(ref dropDownList, dataTable, textField, valueField, ND.Data.DataObject.SortType.None, false);
        }

        public static Boolean PopulateDropDownList(ref DropDownList dropDownList, DataTable dataTable, String textField, String valueField, ND.Data.DataObject.SortType sortType)
        {
            return Form.PopulateDropDownList(ref dropDownList, dataTable, textField, valueField, sortType, false);
        }

        public static Boolean PopulateDropDownList(ref DropDownList dropDownList, DataTable dataTable, String textField, String valueField, ND.Data.DataObject.SortType sortType, Boolean firstItemBlank)
        {
            Boolean result = false;
            try
            {
                dropDownList.DataSource = null;
                dropDownList.Items.Clear();
                if (dataTable != null)
                {
                    DataTable newDataTable = Form.SortData(dataTable, sortType, textField);
                    if (firstItemBlank) ND.Data.DataObject.AddEmptyDataRow(ref newDataTable);
                    dropDownList.DataTextField = textField;
                    dropDownList.DataValueField = valueField;
                    dropDownList.DataSource = newDataTable;
                    dropDownList.DataBind();
                    result = true;
                }                
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion

        #region DataGrid

        public static Boolean PopulateDataGridView(ref DataGrid dataGrid, DataTable dataTable)
        {
            return Form.PopulateDataGridView(ref dataGrid, dataTable, ND.Data.DataObject.SortType.None, String.Empty);
        }

        public static Boolean PopulateDataGridView(ref DataGrid dataGrid, DataTable dataTable, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                dataGrid.DataSource = null;
                if (dataTable != null)
                {
                    DataTable newDataTable = SortData(dataTable, sortType, sortField);
                    dataGrid.DataSource = newDataTable;
                    dataGrid.DataBind();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion

        #region ListBox

        public static Boolean PopulateListBox(ref ListBox listBox, DataTable dataTable, String textField, String valueField)
        {
            return Form.PopulateListBox(ref listBox, dataTable, textField, valueField, ND.Data.DataObject.SortType.None, false);
        }

        public static Boolean PopulateListBox(ref ListBox listBox, DataTable dataTable, String textField, String valueField, ND.Data.DataObject.SortType sortType, Boolean firstItemBlank)
        {
            Boolean result = false;
            try
            {
                listBox.DataSource = null;
                listBox.Items.Clear();
                if (dataTable != null)
                {
                    DataTable newDataTable = Form.SortData(dataTable, sortType, textField);
                    if (firstItemBlank) ND.Data.DataObject.AddEmptyDataRow(ref newDataTable);
                    listBox.DataTextField = textField;
                    listBox.DataValueField = valueField;
                    listBox.DataSource = newDataTable;
                    listBox.DataBind();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion

        #region ListView

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable)
        {
            return Form.PopulateListView(ref listView, dataTable, Data.DataObject.SortType.None, String.Empty);
        }

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable, ND.Data.DataObject.SortType sortType, String sortField)
        {
            return Form.PopulateListView(ref listView, dataTable, sortType, sortField, false, String.Empty, String.Empty, String.Empty);
        }

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable, ND.Data.DataObject.SortType sortType, String sortField, Boolean sortReference, String parentField, String referenceField, String indentField)
        {
            Boolean result = false;
            try
            {                
                if (dataTable != null)
                {
                    DataTable newDataTable = Form.SortData(dataTable, sortType, sortField);
                    listView.DataSource = newDataTable;
                    listView.DataBind();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion

        #region TreeView

        public static Boolean PopulateTreeView(ref TreeView treeView, DataTable dataTable, String displayField, String idField, String parentField)
        {
            return Form.PopulateTreeView(ref treeView, dataTable, displayField, idField, parentField, String.Empty, null, true);
        }

        public static Boolean PopulateTreeView(ref TreeView treeView, DataTable dataTable, String displayField, String idField, String parentField, String title)
        {
            return Form.PopulateTreeView(ref treeView, dataTable, displayField, idField, parentField, title, null, true);
        }

        public static Boolean PopulateTreeView(ref TreeView treeView, DataTable dataTable, String displayField, String idField, String parentField, String title, TreeNode parentNode, Boolean isNew)
        {
            Boolean result = false;
            try
            {
                if (isNew)
                {
                    treeView.Nodes.Clear();
                    treeView.Nodes.Add(new TreeNode(title));
                    parentNode = treeView.Nodes[0];
                    parentNode.Value = "0";
                }
                DataRow[] dataRow = dataTable.Select("[" + parentField + "]='" + parentNode.Value.ToString() + "'");
                foreach (DataRow row in dataRow)
                {
                    TreeNode node = new TreeNode(row[displayField].ToString());
                    parentNode.ChildNodes.Add(node);
                    node.Value = row[idField].ToString();
                    Form.PopulateTreeView(ref treeView, dataTable, displayField, idField, parentField, String.Empty, node, false);
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion
    }
}
