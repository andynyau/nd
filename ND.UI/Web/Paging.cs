﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ND.UI.Web
{
    public class Paging
    {
        public static List<ListItem> Populate(int recordPerPage, int pagerPerPage, int recordCount, int currentPage)
        {
            return Paging.Populate(recordPerPage, pagerPerPage, recordCount, currentPage, "First", "Previous", "Next", "Last");
        }

        public static List<ListItem> Populate(int recordPerPage, int pagerPerPage, int recordCount, int currentPage, string firstText, string previousText, string nextText, string lastText)
        {
            List<ListItem> pages = new List<ListItem>();
            try
            {
                double dblPageCount = (double)((decimal)recordCount / recordPerPage);
                int pageCount = (int)Math.Ceiling(dblPageCount);
                if (pageCount > 0)
                {
                    pages.Add(new ListItem(firstText, "1", currentPage > 1));
                    pages.Add(new ListItem(previousText, (currentPage - 1).ToString(), currentPage > 1));
                    int repeaterStart = 1;
                    if (currentPage < pagerPerPage + 1)
                    {
                        if (currentPage == pagerPerPage)
                        {
                            repeaterStart = 2;
                        }
                        else
                        {
                            repeaterStart = 1;
                        }
                    }
                    else if (currentPage > (pageCount / pagerPerPage) * pagerPerPage)
                    {
                        if (currentPage == (int)(pageCount / pagerPerPage) * pagerPerPage + 1)
                        {
                            repeaterStart = (int)(pageCount / pagerPerPage) * pagerPerPage;
                        }
                        else
                        {
                            repeaterStart = (int)(pageCount / pagerPerPage) * pagerPerPage + 1;
                        }
                    }
                    else if ((currentPage % pagerPerPage) == 0)
                    {
                        repeaterStart = (int)((currentPage - 1) / pagerPerPage) * pagerPerPage + 2;
                    }
                    else
                    {
                        if (currentPage == (int)(currentPage / pagerPerPage) * pagerPerPage + 1)
                        {
                            repeaterStart = (int)(currentPage / pagerPerPage) * pagerPerPage;
                        }
                        else
                        {
                            repeaterStart = (int)(currentPage / pagerPerPage) * pagerPerPage + 1;
                        }
                    }
                    for (int i = repeaterStart; i < (repeaterStart + pagerPerPage > pageCount ? pageCount + 1 : repeaterStart + pagerPerPage); i++) pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage));
                    pages.Add(new ListItem(nextText, (currentPage + 1).ToString(), currentPage < pageCount));
                    pages.Add(new ListItem(lastText, pageCount.ToString(), currentPage < pageCount));
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());                
            }            
            return pages;
        }        
    }
}
