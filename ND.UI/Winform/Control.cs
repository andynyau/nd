﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ND.UI.Winform
{
    public class Control
    {
        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new System.Windows.Forms.Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public static Boolean AddHeaderCheckBox(ref ListView listView)
        {
            Boolean result = false;
            try
            {
                listView.DrawColumnHeader += new DrawListViewColumnHeaderEventHandler(Control._ListView_DrawColumnHeader);
                listView.DrawItem += new DrawListViewItemEventHandler(Control._ListView_DrawItem);
                listView.DrawSubItem += new DrawListViewSubItemEventHandler(Control._ListView_DrawSubItem);
                listView.OwnerDraw = true;
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        private static void _ListView_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    ListView listView = ((ListView)sender);
                    listView.SuspendLayout();
                    e.DrawBackground();
                    CheckBox checkBox = new CheckBox();
                    checkBox.Text = String.Empty;
                    checkBox.Size = new Size(15, 15);
                    checkBox.Name = "chkSelectAll";
                    checkBox.ThreeState = false;
                    checkBox.Checked = false;
                    checkBox.CheckedChanged += new EventHandler(Control._CheckBox_CheckedChanged);
                    checkBox.Location = new System.Drawing.Point(4, 4);
                    listView.Controls.Add(checkBox);
                    checkBox.Show();
                    checkBox.BringToFront();
                    e.DrawText(TextFormatFlags.VerticalCenter);
                    listView.ResumeLayout();
                }
                else
                {
                    e.DrawDefault = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private static void _ListView_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            try
            {
                e.DrawDefault = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private static void _ListView_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            try
            {
                e.DrawDefault = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private static void _CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox checkBox = (CheckBox)sender;
                ListView listView = (ListView)checkBox.Parent;

                int count = 0;
                for (count = 0; count < listView.Items.Count; count++)
                {
                    listView.Items[count].Checked = checkBox.Checked;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }
    }
}
