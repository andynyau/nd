﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace ND.UI.Winform
{
    public class Form : UiBase
    {
        #region Declaration

        public delegate void DelegateLoadDataMethod(ND.Data.DataObject.SortType sortType, String sortField);

        #endregion

        #region ComboBox

        public static Boolean PopulateComboBox(ref ComboBox comboBox, DataTable dataTable, String textField, String valueField)
        {
            return Form.PopulateComboBox(ref comboBox, dataTable, textField, valueField, ND.Data.DataObject.SortType.None, false);
        }

        public static Boolean PopulateComboBox(ref ComboBox comboBox, DataTable dataTable, String textField, String valueField, ND.Data.DataObject.SortType sortType)
        {
            return Form.PopulateComboBox(ref comboBox, dataTable, textField, valueField, sortType, false);
        }

        public static Boolean PopulateComboBox(ref ComboBox comboBox, DataTable dataTable, String textField, String valueField, ND.Data.DataObject.SortType sortType, Boolean firstItemBlank)
        {
            Boolean result = false;
            try
            {
                comboBox.DataSource = null;
                comboBox.Items.Clear();
                if (dataTable != null)
                {
                    DataTable newDataTable = Form.SortData(dataTable, sortType, textField);
                    if (firstItemBlank) ND.Data.DataObject.AddEmptyDataRow(ref newDataTable);
                    comboBox.DisplayMember = textField;
                    comboBox.ValueMember = valueField;
                    comboBox.DataSource = newDataTable;
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }
        
        #endregion

        #region ListBox
        
        public static Boolean PopulateListBox(ref ListBox listBox, DataTable dataTable, String textField, String valueField)
        {
            return Form.PopulateListBox(ref listBox, dataTable, textField, valueField, ND.Data.DataObject.SortType.None, false);
        }

        public static Boolean PopulateListBox(ref ListBox listBox, DataTable dataTable, String textField, String valueField, ND.Data.DataObject.SortType sortType, Boolean firstItemBlank)
        {
            Boolean result = false;
            try
            {
                listBox.DataSource = null;
                listBox.Items.Clear();
                if (dataTable != null)
                {
                    DataTable newDataTable = Form.SortData(dataTable, sortType, textField);
                    if (firstItemBlank) ND.Data.DataObject.AddEmptyDataRow(ref newDataTable);
                    listBox.DisplayMember = textField;
                    listBox.ValueMember = valueField;
                    listBox.DataSource = newDataTable;                    
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }
        
        #endregion

        #region ListView

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable)
        {
            return Form.PopulateListView(ref listView, dataTable, false);
        }

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable, Boolean checkBox)
        {
            return Form.PopulateListView(ref listView, dataTable, checkBox, false);
        }

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable, Boolean checkBox, Boolean generateColumn)
        {
            return Form.PopulateListView(ref listView, dataTable, checkBox, generateColumn, ND.Data.DataObject.SortType.None, String.Empty);
        }

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable, Boolean checkBox, Boolean generateColumn, ND.Data.DataObject.SortType sortType, String sortField)
        {
            return Form.PopulateListView(ref listView, dataTable, checkBox, generateColumn, sortType, sortField, false, String.Empty, String.Empty, String.Empty);
        }

        public static Boolean PopulateListView(ref ListView listView, DataTable dataTable, Boolean checkBox, Boolean generateColumn, ND.Data.DataObject.SortType sortType, String sortField, Boolean sortReference, String parentField, String referenceField, String indentField)
        {
            Boolean result = false;
            try
            {
                listView.Items.Clear();
                if (dataTable != null)
                {
                    if ((listView.Columns[0].Name == "CheckBox")) listView.Columns.RemoveAt(0);
                    if (generateColumn)
                    {
                        Form.GenerateListViewColumn(listView, dataTable);
                    }
                    else
                    {
                        if (listView.Columns.Count != dataTable.Columns.Count) Form.GenerateListViewColumn(listView, dataTable);
                    }
                    if (checkBox == true)
                    {
                        listView.Columns.Insert(0, "CheckBox", String.Empty);
                        listView.Columns[0].Width = 20;
                        Control.AddHeaderCheckBox(ref listView);
                    }
                    else
                    {
                        listView.OwnerDraw = false;
                        listView.Controls.Clear();
                    }
                    listView.CheckBoxes = checkBox;
                    DataTable newDataTable = Form.SortData(dataTable, sortType, sortField);
                    int rowCount = 0;
                    foreach (DataRow dataRow in newDataTable.Rows)
                    {
                        if (((sortReference == true) && ((int)dataRow[referenceField] == 0)) || (sortReference == false))
                        {
                            if (checkBox == true)
                            {
                                listView.Items.Add(String.Empty);
                                int columnCount = 0;
                                for (columnCount = 0; columnCount < newDataTable.Columns.Count; columnCount++) listView.Items[rowCount].SubItems.Add(dataRow[columnCount].ToString());
                            }
                            else
                            {
                                listView.Items.Add(dataRow[0].ToString());
                                int columnCount = 0;
                                for (columnCount = 1; columnCount < newDataTable.Columns.Count; columnCount++) listView.Items[rowCount].SubItems.Add(dataRow[columnCount].ToString());
                            }
                            rowCount++;
                            if (sortReference == true)
                            {
                                foreach (DataRow dataRowRef in newDataTable.Rows)
                                {
                                    if ((int)dataRowRef[referenceField] == (int)dataRow[parentField])
                                    {
                                        if (newDataTable.Columns[0].ColumnName == indentField)
                                        {
                                            listView.Items.Add(" - " + dataRowRef[0].ToString());
                                        }
                                        else
                                        {
                                            listView.Items.Add(dataRowRef[0].ToString());
                                        }
                                        int columnCountRef = 0;
                                        for (columnCountRef = 1; columnCountRef < newDataTable.Columns.Count; columnCountRef++)
                                        {
                                            if (newDataTable.Columns[columnCountRef].ColumnName == indentField)
                                            {
                                                listView.Items[rowCount].SubItems.Add(" - " + dataRowRef[columnCountRef].ToString());
                                            }
                                            else
                                            {
                                                listView.Items[rowCount].SubItems.Add(dataRowRef[columnCountRef].ToString());
                                            }
                                        }
                                        rowCount++;
                                    }
                                }
                            }
                        }
                    }
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public static Boolean SortListView(ref ListView listView, ref String currentSort, int columnIndex, DelegateLoadDataMethod loadDataMethod)
        {
            return Form.SortListView(ref listView, ref currentSort, columnIndex, ND.Data.DataObject.SortType.None, String.Empty, loadDataMethod);
        }

        public static Boolean SortListView(ref ListView listView, ref String currentSort, int columnIndex, ND.Data.DataObject.SortType defaultSortType, String defaultSortField, DelegateLoadDataMethod loadDataMethod)
        {
            Boolean result = false;
            try
            {
                ND.Data.DataObject.SortType sortType = ND.Data.DataObject.SortType.None;
                String sortField = String.Empty;
                String columnText = String.Empty;
                String[] sort_Info = null;
                if (currentSort != String.Empty) sort_Info = currentSort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                if (currentSort == String.Empty)
                {
                    currentSort = listView.Columns[columnIndex].Name + "[..]ASC[..]" + columnIndex.ToString();
                    sortField = listView.Columns[columnIndex].Name;
                    sortType = ND.Data.DataObject.SortType.Ascending;
                    columnText = listView.Columns[columnIndex].Text.Contains("˄") || listView.Columns[columnIndex].Text.Contains("˅") ? listView.Columns[columnIndex].Text.Replace("˄", "˄").Replace("˅", "˄") : listView.Columns[columnIndex].Text + " ˄";
                }
                else if (sort_Info == null)
                {
                    currentSort = listView.Columns[columnIndex].Name + "[..]ASC[..]" + columnIndex.ToString();
                    sortField = listView.Columns[columnIndex].Name;
                    sortType = ND.Data.DataObject.SortType.Ascending;
                    columnText = listView.Columns[columnIndex].Text.Contains("˄") || listView.Columns[columnIndex].Text.Contains("˅") ? listView.Columns[columnIndex].Text.Replace("˄", "˄").Replace("˅", "˄") : listView.Columns[columnIndex].Text + " ˄";
                }
                else if (sort_Info[0] == listView.Columns[columnIndex].Name)
                {
                    if (sort_Info[1] == "ASC")
                    {
                        currentSort = listView.Columns[columnIndex].Name + "[..]DESC[..]" + columnIndex.ToString();
                        sortField = listView.Columns[columnIndex].Name;
                        sortType = ND.Data.DataObject.SortType.Descending;
                        columnText = listView.Columns[columnIndex].Text.Contains("˄") || listView.Columns[columnIndex].Text.Contains("˅") ? listView.Columns[columnIndex].Text.Replace("˄", "˅").Replace("˅", "˅") : listView.Columns[columnIndex].Text + " ˅";

                    }
                    else if (sort_Info[1] == "DESC")
                    {
                        currentSort = String.Empty;
                        sortField = defaultSortField;
                        sortType = defaultSortType;
                        columnText = listView.Columns[columnIndex].Text.Replace(" ˅", String.Empty).Replace(" ˄", String.Empty);
                    }
                }
                else if (sort_Info[0] != listView.Columns[columnIndex].Name)
                {
                    currentSort = listView.Columns[columnIndex].Name + "[..]ASC[..]" + columnIndex.ToString();
                    sortField = listView.Columns[columnIndex].Name;
                    sortType = ND.Data.DataObject.SortType.Ascending;
                    columnText = listView.Columns[columnIndex].Text.Contains("˄") || listView.Columns[columnIndex].Text.Contains("˅") ? listView.Columns[columnIndex].Text.Replace("˄", "˄").Replace("˅", "˄") : listView.Columns[columnIndex].Text + " ˄";
                }
                loadDataMethod.Invoke(sortType, sortField);
                listView.Columns[columnIndex].Text = columnText;
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public static Boolean FormatListView(ref ListView listView, int columnIndex, Type dataType, String formatString)
        {
            Boolean result = false;
            try
            {
                foreach (ListViewItem listViewItem in listView.Items)
                {
                    if (dataType == typeof(System.DateTime))
                    {
                        listViewItem.SubItems[columnIndex].Text = String.Format(formatString, System.DateTime.Parse(listViewItem.SubItems[columnIndex].Text));
                    }
                    else if (dataType == typeof(System.Decimal))
                    {
                        listViewItem.SubItems[columnIndex].Text = String.Format(formatString, System.Decimal.Parse(listViewItem.SubItems[columnIndex].Text));
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion

        #region DataGridView
        
        public static Boolean PopulateDataGridView(ref DataGridView dataGridView, DataTable dataTable)
        {
            return Form.PopulateDataGridView(ref dataGridView, dataTable, ND.Data.DataObject.SortType.None, String.Empty);
        }

        public static Boolean PopulateDataGridView(ref DataGridView dataGridView, DataTable dataTable, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                dataGridView.DataSource = null;
                if (dataTable != null)
                {
                    DataTable newDataTable = SortData(dataTable, sortType, sortField);
                    dataGridView.DataSource = newDataTable;
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }
        
        #endregion

        #region TreeView

        public static Boolean PopulateTreeView(ref TreeView treeView, DataTable dataTable, String displayField, String idField, String parentField)
        {
            return Form.PopulateTreeView(ref treeView, dataTable, displayField, idField, parentField, String.Empty, null, true);
        }

        public static Boolean PopulateTreeView(ref TreeView treeView, DataTable dataTable, String displayField, String idField, String parentField, String title)
        {
            return Form.PopulateTreeView(ref treeView, dataTable, displayField, idField, parentField, title, null, true);
        }

        public static Boolean PopulateTreeView(ref TreeView treeView, DataTable dataTable, String displayField, String idField, String parentField, String title, TreeNode parentNode, Boolean isNew)
        {
            Boolean result = false;
            try
            {
                if (isNew)
                {
                    treeView.Nodes.Clear();
                    treeView.Nodes.Add(new TreeNode(title));
                    parentNode = treeView.Nodes[0];
                    parentNode.Tag = "0";
                }
                DataRow[] dataRow = dataTable.Select("[" + parentField + "]='" + parentNode.Tag.ToString() + "'");
                foreach (DataRow row in dataRow)
                {
                    TreeNode node = new TreeNode(row[displayField].ToString());
                    parentNode.Nodes.Add(node);
                    node.Tag = row[idField].ToString();
                    Form.PopulateTreeView(ref treeView, dataTable, displayField, idField, parentField, String.Empty, node, false);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        #endregion

        #region Methods

        private static Boolean GenerateListViewColumn(ListView listView, DataTable dataTable)
        {
            Boolean result = false;
            try
            {
                listView.Columns.Clear();
                foreach (DataColumn dataColumn in dataTable.Columns)
                {
                    listView.Columns.Add(dataColumn.ColumnName.ToString(), dataColumn.ColumnName.ToString());
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        #endregion
    }
}
