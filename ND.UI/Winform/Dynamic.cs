﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Reflection;

namespace ND.UI.Winform
{
    public class Dynamic
    {
        #region Common

        private Boolean FindEventsByName(object sender, object receiver, Boolean bind, String handlerPrefix, String handlerSuffix)
        {
            Boolean result = false;
            try
            {
                EventInfo[] senderEvent = sender.GetType().GetEvents();
                Type receiverType = receiver.GetType();
                System.Reflection.MethodInfo methodInfo;
                foreach (System.Reflection.EventInfo eventInfo in senderEvent)
                {
                    methodInfo = receiverType.GetMethod(String.Format("{0}{1}{2}", handlerPrefix, eventInfo.Name, handlerSuffix), System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                    if (methodInfo != null)
                    {
                        System.Delegate newDelegate = System.Delegate.CreateDelegate(eventInfo.EventHandlerType, receiver, methodInfo.Name);
                        if (bind)
                        {
                            eventInfo.AddEventHandler(sender, newDelegate);
                        }
                        else
                        {
                            eventInfo.RemoveEventHandler(sender, newDelegate);
                        }
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion

        #region MenuStrip

        public Boolean LoadDynamicMenu(ref MenuStrip menuStrip, String xmlPath, Object form, String eventPrefix)
        {
            Boolean result = false;
            try
            {
                XmlNodeList nodelist = ND.Standard.XML.ReadXML(Standard.XML.ReadType.File, xmlPath, "TopLevelMenu");
                foreach (XmlNode node in nodelist)
                {
                    ToolStripMenuItem menuItem = new ToolStripMenuItem();
                    menuItem.Name = node.Attributes["Name"].Value;
                    menuItem.Text = node.Attributes["Text"].Value;
                    menuStrip.Items.Add(menuItem);
                    this.GenerateMenusFromXML(node, (ToolStripMenuItem)menuStrip.Items[menuStrip.Items.Count - 1], form, eventPrefix);
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        private Boolean GenerateMenusFromXML(XmlNode node, ToolStripMenuItem menuItem, Object form, String eventPrefix)
        {
            Boolean result = false;
            try
            {
                ToolStripItem item = null;
                ToolStripSeparator separator = null;
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    if (childNode.Attributes["Text"].Value == "-")
                    {
                        separator = new ToolStripSeparator();
                        menuItem.DropDownItems.Add(separator);
                    }
                    else
                    {
                        item = new ToolStripMenuItem();
                        item.Name = childNode.Attributes["Name"].Value;
                        item.Text = childNode.Attributes["Text"].Value;
                        menuItem.DropDownItems.Add(item);
                        if (childNode.Attributes["FormLocation"] != null) item.Tag = childNode.Attributes["FormLocation"].Value;
                        if (childNode.Attributes["OnClick"] != null) this.FindEventsByName(item, form, true, eventPrefix, childNode.Attributes["OnClick"].Value);
                        this.GenerateMenusFromXML(childNode, (ToolStripMenuItem)menuItem.DropDownItems[menuItem.DropDownItems.Count - 1], form, eventPrefix);
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());               
            }                        
            return result;
        }

        #endregion

        #region ToolStrip

        public Boolean LoadDynamicTool(ref ToolStrip toolStrip, String xmlPath, Object form, String eventPrefix)
        {
            Boolean result = false;
            try
            {
                XmlNodeList nodelist = ND.Standard.XML.ReadXML(Standard.XML.ReadType.File, xmlPath, "Item");
                foreach (XmlNode node in nodelist)
                {
                    ToolStripItem item = null;
                    switch (node.Attributes["Type"].Value)
                    {
                        case "Button":
                            {
                                item = new ToolStripButton();
                                switch (node.Attributes["DisplayStyle"].Value)
                                {
                                    case "ImageAndText":
                                        {
                                            item.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
                                            break;
                                        }
                                    case "Image":
                                        {
                                            item.DisplayStyle = ToolStripItemDisplayStyle.Image;
                                            break;
                                        }
                                    case "Text":
                                        {
                                            item.DisplayStyle = ToolStripItemDisplayStyle.Text;
                                            break;
                                        }
                                    default:
                                        {
                                            item.DisplayStyle = ToolStripItemDisplayStyle.None;
                                            break;
                                        }
                                }
                                switch (node.Attributes["TextImageRelation"].Value)
                                {
                                    case "ImageAboveText":
                                        {
                                            item.TextImageRelation = TextImageRelation.ImageAboveText;
                                            break;
                                        }
                                    case "ImageBeforeText":
                                        {
                                            item.TextImageRelation = TextImageRelation.ImageBeforeText;
                                            break;
                                        }
                                    case "TextAboveImage":
                                        {
                                            item.TextImageRelation = TextImageRelation.TextAboveImage;
                                            break;
                                        }
                                    case "TextBeforeImage":
                                        {
                                            item.TextImageRelation = TextImageRelation.TextBeforeImage;
                                            break;
                                        }
                                    default:
                                        {
                                            item.TextImageRelation = TextImageRelation.Overlay;
                                            break;
                                        }
                                }
                                if (node.Attributes["Image"].Value != String.Empty)
                                {
                                    item.ImageAlign = ContentAlignment.MiddleLeft;
                                    item.Image = Image.FromFile(node.Attributes["Image"].Value);
                                    item.ImageScaling = ToolStripItemImageScaling.None;
                                }
                                    
                                break;
                            }
                        case "Label":
                            {
                                item = new ToolStripLabel();
                                break;
                            }
                        case "-":
                            {
                                item = new ToolStripSeparator();
                                break;
                            }
                    }
                    if (node.Attributes["Name"] != null)
                    {
                        item.Name = node.Attributes["Name"].Value;
                    }
                    if (node.Attributes["Text"] != null)
                    {
                        item.Text = node.Attributes["Text"].Value;
                    }
                    toolStrip.Items.Add(item);
                    if (node.Attributes["OnClick"] != null)
                    {
                        this.FindEventsByName(item, form, true, eventPrefix, node.Attributes["OnClick"].Value);
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());                
            }
            return result;
        }
        #endregion
    }
}
