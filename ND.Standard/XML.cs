﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace ND.Standard
{
    public class XML
    {
        #region Fields

        #endregion

        #region Properties

        public enum ReadType
        {
            Content = 1,
            File = 2
        }

        #endregion

        #region Methods

        public static XmlNodeList ReadXML(ReadType readType, String content)
        {
            return ReadXML(readType, content, String.Empty);
        }

        public static XmlNodeList ReadXML(ReadType readType, String content, String nodeID)
        {
            XmlNodeList result = null;
            try
            {
                if (content != String.Empty)
                {
                    XmlDocument document = new XmlDocument();
                    switch (readType)
                    {
                        case ReadType.Content:
                            {
                                document.LoadXml(content);
                                break;
                            }
                        case ReadType.File:
                            {
                                document.Load(content);
                                break;
                            }
                    }
                    if (nodeID == String.Empty) nodeID = document.LastChild.Name;
                    result = document.GetElementsByTagName(nodeID);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion
    }
}
