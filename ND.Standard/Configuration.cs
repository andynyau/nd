﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Configuration;

namespace ND.Standard
{
    public class Configuration
    {
        #region Fields

        private String _ConfigFile;

        #endregion

        #region Properties

        public String ConfigFile { get { return this._ConfigFile; } set { this._ConfigFile = value; } }

        #endregion

        #region Methods

        public Boolean CheckConfigFileExist()
        {
            Boolean result = false;
            try
            {
                if (File.Exists(this._ConfigFile)) result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean CreateConfigFile()
        {
            Boolean result = false;
            try
            {
                if (!File.Exists(this._ConfigFile))
                {
                    using (StreamWriter sw = File.CreateText(this._ConfigFile))
                    {
                        sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                        sw.WriteLine("<configuration>");
                        sw.WriteLine("  <appSettings>");
                        sw.WriteLine("    <!--   User application and configured property settings go here.-->");
                        sw.WriteLine("    <!--   Example: <add key=\"settingName\" value=\"settingValue\"/> -->");
                        sw.WriteLine("  </appSettings>");
                        sw.WriteLine("</configuration>");
                        sw.Close();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean CheckConfigElement(Hashtable configurationElements)
        {
            Boolean result = false;
            try
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(this._ConfigFile.Replace(".config", String.Empty));
                foreach (DictionaryEntry element in configurationElements)
                {
                    Boolean found = false;
                    foreach (String key in config.AppSettings.Settings.AllKeys)
                    {
                        if (key == element.Key.ToString())
                        {
                            found = true; 
                        }
                    }
                    if (found == false)
                    {
                        config.AppSettings.Settings.Add(element.Key.ToString(), String.Empty);
                    }
                }
                config.Save();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion
    }
}
