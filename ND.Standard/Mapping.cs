﻿using System;
using System.Text;
using System.Xml;

namespace ND.Standard
{
    public class Mapping
    {
        public static String[,] LoadMapping(String xmlPath, String entity)
        {
            String[,] mapping = null;
            try
            {
                XmlNodeList nodelist = XML.ReadXML(Standard.XML.ReadType.File, xmlPath, entity);
                mapping = new String[nodelist[0].ChildNodes.Count, 2];
                int count = 0;
                foreach (XmlNode node in nodelist[0].ChildNodes)
                {
                    mapping[count, 0] = node.Attributes["Property"].Value;
                    mapping[count, 1] = node.Attributes["Column"].Value;
                    count++;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return mapping;
        }
    }
}
