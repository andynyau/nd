﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ND.Standard.Validation.Winform
{
    public class Field
    {
        #region Fields

        private object _Control;
        private String _ErrorMessage;
        private ErrorProvider _ErrorProvider;
        private FieldValidationType _ValidationType;
        private String _RegexPattern = String.Empty;
        private DelegateCustomFunction _CustomFunction;

        #endregion

        #region Properties

        public enum FieldValidationType
        {
            Required = 1,
            Regex = 2,
            Custom = 3
        }

        public object Control { get { return this._Control; } set { this._Control = value; } }
        public String ErrorMessage { get { return this._ErrorMessage; } set { this._ErrorMessage = value; } }
        public ErrorProvider ErrorProvider { get { return this._ErrorProvider; } set { this._ErrorProvider = value; } }
        public FieldValidationType ValidationType { get { return this._ValidationType; } set { this._ValidationType = value; } }
        public String RegexPattern { get { return this._RegexPattern; } set { this._RegexPattern = value; } }
        public delegate Boolean DelegateCustomFunction();
        public DelegateCustomFunction CustomFunction { get { return this._CustomFunction; } set { this._CustomFunction = value; } }

        #endregion        
    }
}
