﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ND.Standard.Validation.Winform
{
    public class Form
    {
        #region Fields

        private List<Field> _Fields;

        #endregion

        #region Properties

        public List<Field> Fields { get { return this._Fields; } set { this._Fields = value; } }

        #endregion        

        #region Methods

        public Form()
        {
            this._Fields = new List<Field>();
        }

        public Boolean AddValidator(object control, ErrorProvider errorProvider, Field.FieldValidationType validationType)
        {
            return this.AddValidator(control, String.Empty, errorProvider, validationType);
        }

        public Boolean AddValidator(object control, String errorMessage, ErrorProvider errorProvider, Field.FieldValidationType validationType)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.ErrorMessage = errorMessage;
                validationField.ErrorProvider = errorProvider;
                validationField.ValidationType = validationType;
                validationField.RegexPattern = String.Empty;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddValidator(object control, ErrorProvider errorProvider, Field.FieldValidationType validationType, String regexPattern)
        {
            return this.AddValidator(control, String.Empty, errorProvider, validationType, regexPattern);
        }

        public Boolean AddValidator(object control, String errorMessage, ErrorProvider errorProvider, Field.FieldValidationType validationType, String regexPattern)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.ErrorMessage = errorMessage;
                validationField.ErrorProvider = errorProvider;
                validationField.ValidationType = validationType;
                validationField.RegexPattern = regexPattern;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddValidator(object control, ErrorProvider errorProvider, Field.FieldValidationType validationType, Field.DelegateCustomFunction customFunction)
        {
            return this.AddValidator(control, String.Empty, errorProvider, validationType, customFunction);
        }

        public Boolean AddValidator(object control, String errorMessage, ErrorProvider errorProvider, Field.FieldValidationType validationType, Field.DelegateCustomFunction customFunction)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.ErrorMessage = errorMessage;
                validationField.ErrorProvider = errorProvider;
                validationField.ValidationType = validationType;
                validationField.RegexPattern = String.Empty;
                validationField.CustomFunction = customFunction;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean Validate()
        {
            Boolean result = false;
            try
            {
                foreach (Field field in this._Fields)
                {
                    field.ErrorProvider.Clear();
                }
                foreach (Field field in this._Fields)
                {
                    String errorMessage = String.Empty;
                    String value = String.Empty;
                    if (field.Control.GetType() == typeof(TextBox))
                    {
                        TextBox control = (TextBox)field.Control;
                        value = control.Text.Trim();
                    }
                    else if (field.Control.GetType() == typeof(ComboBox))
                    {
                        ComboBox control = (ComboBox)field.Control;
                        value = control.Text.Trim();
                    }
                    else if (field.Control.GetType() == typeof(DateTimePicker))
                    {
                        DateTimePicker control = (DateTimePicker)field.Control;
                        value = control.Value.ToString().Trim();
                    }
                    switch (field.ValidationType)
                    {
                        case Field.FieldValidationType.Required:
                            {
                                errorMessage = field.ErrorMessage.Trim() == String.Empty ? "Required" : field.ErrorMessage;
                                if (value != String.Empty)
                                {
                                    result = true;
                                }
                                else
                                {
                                    field.ErrorProvider.SetError((Control)field.Control, errorMessage);
                                }
                                break;
                            }
                        case Field.FieldValidationType.Regex:
                            {
                                errorMessage = field.ErrorMessage.Trim() == String.Empty ? "Invalid Format" : field.ErrorMessage;
                                if (Regex.IsMatch(value, field.RegexPattern) == true)
                                {
                                    result = true;
                                }
                                else
                                {
                                    field.ErrorProvider.SetError((Control)field.Control, errorMessage);
                                }
                                break;
                            }
                        case Field.FieldValidationType.Custom:
                            {
                                errorMessage = field.ErrorMessage.Trim() == String.Empty ? "Invalid" : field.ErrorMessage;
                                if (field.CustomFunction.Invoke() == true)
                                {
                                    result = true;
                                }
                                else
                                {
                                    field.ErrorProvider.SetError((Control)field.Control, errorMessage);
                                }
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion
    }
}
