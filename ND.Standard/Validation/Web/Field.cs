﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;

namespace ND.Standard.Validation.Web
{
    public class Field
    {
        #region Fields

        private WebControl _Control;
        private BaseValidator _Validator;        
        private ValidationSummary _ValidationSummary;

        #endregion

        #region Properties

        public enum FieldValidationType
        {
            Required = 1,
            Regex = 2,
            Custom = 3
        }

        public WebControl Control { get { return this._Control; } set { this._Control = value; if (this._Validator != null) this._Validator.ControlToValidate = value.ID; } }
        public BaseValidator Validator { get { return this._Validator; } set { this._Validator = value; if (this._Control != null) this._Validator.ControlToValidate = this._Control.ID; } }
        public String ErrorMessage { get { return this.Validator.ErrorMessage; } set { this.Validator.ErrorMessage = value; } }
        public ValidationSummary ValidationSummary { get { return this._ValidationSummary; } set { this._ValidationSummary = value; } }
        public String ValidationGroup { get { return this._Validator.ValidationGroup; } set { this._Validator.ValidationGroup = value; } }
        public String ControlToCompare { get { return ((CompareValidator)this._Validator).ControlToCompare; } set { ((CompareValidator)this._Validator).ControlToCompare = value; } }
        public String ValueToCompare { get { return ((CompareValidator)this._Validator).ValueToCompare; } set { ((CompareValidator)this._Validator).ValueToCompare = value; } }
        public String ValidationExpression { get { return ((RegularExpressionValidator)this._Validator).ValidationExpression; } set { ((RegularExpressionValidator)this._Validator).ValidationExpression = value; } }
        public String MinimumValue { get { return ((RangeValidator)this._Validator).MinimumValue; } set { ((RangeValidator)this._Validator).MinimumValue = value; } }
        public String MaximumValue { get { return ((RangeValidator)this._Validator).MaximumValue; } set { ((RangeValidator)this._Validator).MaximumValue = value; } }
        public ServerValidateEventHandler ServerValidation { set { ((CustomValidator)this._Validator).ServerValidate += value; } }

        #endregion  

    }
}
