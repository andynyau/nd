﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ND.Standard.Validation.Web
{
    public class Form
    {
        #region Fields

        private List<Field> _Fields;

        #endregion

        #region Properties

        public List<Field> Fields { get { return this._Fields; } set { this._Fields = value; } }

        #endregion

        #region Methods

        public Form()
        {
            this._Fields = new List<Field>();
        }

        public Boolean AddValidator(WebControl control, CompareValidator validator, String errorMessage, ValidationSummary validationSummary, String validationGroup, object controlToCompare)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.Validator = validator;
                validationField.ErrorMessage = errorMessage;
                validationField.ValidationSummary = validationSummary;
                validationField.ValidationGroup = validationGroup;
                validationField.ControlToCompare = ((WebControl)controlToCompare).ID;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddValidator(WebControl control, CompareValidator validator, String errorMessage, ValidationSummary validationSummary, String validationGroup, String valueToCompare)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.Validator = validator;
                validationField.ErrorMessage = errorMessage;
                validationField.ValidationSummary = validationSummary;
                validationField.ValidationGroup = validationGroup;
                validationField.ValueToCompare = valueToCompare;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddValidator(WebControl control, CustomValidator validator, String errorMessage, ValidationSummary validationSummary, String validationGroup, ServerValidateEventHandler serverValidation)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.Validator = validator;
                validationField.ErrorMessage = errorMessage;
                validationField.ValidationSummary = validationSummary;
                validationField.ValidationGroup = validationGroup;
                validationField.ServerValidation = serverValidation;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddValidator(WebControl control, RangeValidator validator, String errorMessage, ValidationSummary validationSummary, String validationGroup, String minimumValue, String maximumValue)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.Validator = validator;
                validationField.ErrorMessage = errorMessage;
                validationField.ValidationSummary = validationSummary;
                validationField.ValidationGroup = validationGroup;
                validationField.MinimumValue = minimumValue;
                validationField.MaximumValue = maximumValue;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddValidator(WebControl control, RegularExpressionValidator validator, String errorMessage, ValidationSummary validationSummary, String validationGroup, String validationExpression)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.Validator = validator;
                validationField.ErrorMessage = errorMessage;
                validationField.ValidationSummary = validationSummary;
                validationField.ValidationGroup = validationGroup;
                validationField.ValidationExpression = validationExpression;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddValidator(WebControl control, RequiredFieldValidator validator, String errorMessage, ValidationSummary validationSummary, String validationGroup)
        {
            Boolean result = false;
            try
            {
                Field validationField = new Field();
                validationField.Control = control;
                validationField.Validator = validator;
                validationField.ErrorMessage = errorMessage;
                validationField.ValidationSummary = validationSummary;
                validationField.ValidationGroup = validationGroup;
                this._Fields.Add(validationField);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion
    }
}
