﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Web;

namespace ND.Log
{
    public class LogWriter
    {
        public static Boolean WriteLog(String message)
        {
            Boolean result = false;
            try
            {
                String fileName = "Error.log";
                String logPath = String.Empty;
                if (ConfigurationManager.AppSettings["SET:LOGPATH"] != null)
                {
                    logPath = ConfigurationManager.AppSettings["SET:LOGPATH"].ToString();
                    if (logPath.Contains(":") == false) logPath = LogWriter.GetDefaultLogPath() + logPath;
                }
                else
                {
                    logPath = LogWriter.GetDefaultLogPath();
                }
                if (!Directory.Exists(logPath)) Directory.CreateDirectory(logPath);
                if (!File.Exists(logPath + fileName))
                {
                    using (StreamWriter streamWriter = File.CreateText(logPath + fileName))
                    {
                        streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt") + " : " + message);
                        streamWriter.WriteLine("");
                        streamWriter.Close();
                    }
                }
                else
                {
                    using (StreamWriter streamWriter = File.AppendText(logPath + fileName))
                    {
                        streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt") + " : " + message);
                        streamWriter.WriteLine("");
                        streamWriter.Close();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return result;
        }

        private static string GetDefaultLogPath()
        {
            String logPath = String.Empty;
            if (HttpContext.Current != null)
            {
                logPath = Path.GetDirectoryName(HttpContext.Current.Request.MapPath("/")) + "\\";
            }
            else
            {
                logPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\";
            }
            return logPath;
        }
    }
}
