﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Configuration;

namespace ND.Data.OleDb
{
    public class DataAccess
    {
        #region Fields

        private OleDbConnection _Connection;
        private OleDbCommand _Command;
        private OleDbDataReader _DataReader;
        private List<OleDbParameter> _Parameters;
        private DataTable _DataTable;
        private OleDbDataAdapter _DataAdapter;
        private DataSet _DataSet;
        private OleDbTransaction _Transaction;

        #endregion

        #region Properties

        public OleDbConnection Connection { get { return this._Connection; } set { this._Connection = value; } }
        public List<OleDbParameter> Parameters { get { return this._Parameters; } set { this._Parameters = value; } }

        #endregion

        #region Methods

        public DataAccess()
        {
            this._Connection = new OleDbConnection();
            this._Connection.ConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString;
            this._Command = new OleDbCommand();
            this._Parameters = new List<OleDbParameter>();
            this._DataTable = new DataTable();
            this._DataAdapter = new OleDbDataAdapter();
            this._DataSet = new DataSet();
        }

        public DataAccess(string connString)
        {
            this._Connection = new OleDbConnection();
            this._Connection.ConnectionString = connString;
            this._Command = new OleDbCommand();
            this._Parameters = new List<OleDbParameter>();
            this._DataTable = new DataTable();
            this._DataAdapter = new OleDbDataAdapter();
            this._DataSet = new DataSet();
        }

        public Boolean TryConnect()
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
            }
            return result;
        }

        public Boolean ExecuteQuery(String commandText, CommandType commandType)
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.CommandText = commandText;
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (OleDbParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._Transaction = this._Connection.BeginTransaction(IsolationLevel.Serializable);
                this._Command.Transaction = this._Transaction;
                this._Command.ExecuteNonQuery();
                this._Transaction.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                this._Transaction.Rollback();
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                this._Parameters.Clear();
            }
            return result;
        }

        public Boolean BatchExecuteQuery(ArrayList commandTexts, CommandType commandType)
        {            
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (OleDbParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._Transaction = this._Connection.BeginTransaction(IsolationLevel.Serializable);
                this._Command.Transaction = this._Transaction;
                if (commandTexts != null)
                {
                    foreach (String commandText in commandTexts)
                    {
                        this._Command.CommandText = commandText;
                        this._Command.ExecuteNonQuery();
                    }
                }
                this._Transaction.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                this._Transaction.Rollback();
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                this._Parameters.Clear();
                commandTexts.Clear();
            }
            return result;
        }

        public Boolean ExecuteQueryNoTrans(String commandText, CommandType commandType)
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.CommandText = commandText;
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (OleDbParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._Command.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                this._Parameters.Clear();
            }
            return result;
        }

        public Boolean BatchExecuteQueryNoTrans(ArrayList commandTexts, CommandType commandType)
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (OleDbParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                if (commandTexts != null)
                {
                    foreach (String commandText in commandTexts)
                    {
                        this._Command.CommandText = commandText;
                        this._Command.ExecuteNonQuery();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                this._Parameters.Clear();
                commandTexts.Clear();
            }
            return result;
        }

        public DataTable LoadData(String commandText, CommandType commandType)
        {
            DataTable result = null;
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                this._Command.CommandText = commandText;
                if (this._Parameters != null) foreach (OleDbParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._DataReader = this._Command.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                this._DataTable = new DataTable();
                this._DataTable.Load(this._DataReader);
                result = this._DataTable.Copy();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                this._Parameters.Clear();
            }
            return result;
        }

        public DataSet LoadMultipleData(String commandText, CommandType commandType)
        {
            DataSet result = new DataSet();
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                this._Command.CommandText = commandText;
                if (this._Parameters != null) foreach (OleDbParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._DataAdapter.SelectCommand = this._Command;
                this._DataAdapter.Fill(this._DataSet);
                result = this._DataSet.Copy();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                this._Parameters.Clear();
            }
            return result;
        }

        #endregion
    }
}
