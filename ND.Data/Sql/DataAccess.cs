﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ND.Data.SQL
{
    public class DataAccess
    {
        #region Fields

        private SqlConnection _Connection;
        private SqlCommand _Command;
        private SqlDataReader _DataReader;
        private List<SqlParameter> _Parameters;
        private DataTable _DataTable;
        private SqlDataAdapter _DataAdapter;
        private DataSet _DataSet;
        private SqlTransaction _Transaction;
        private Boolean _ClearParameters;

        #endregion

        #region Properties

        public SqlConnection Connection { get { return this._Connection; } set { this._Connection = value; } }
        public List<SqlParameter> Parameters { get { return this._Parameters; } set { this._Parameters = value; } }
        public Boolean ClearParameters { get { return this._ClearParameters; } set { this._ClearParameters = value; } }

        #endregion

        #region Methods

        public DataAccess()
        {
            this._Connection = new SqlConnection();
            this._Connection.ConnectionString = ConfigurationManager.ConnectionStrings[1].ConnectionString;
            this._Command = new SqlCommand();
            this._Parameters = new List<SqlParameter>();
            this._DataTable = new DataTable();
            this._DataAdapter = new SqlDataAdapter();
            this._DataSet = new DataSet();
            this._ClearParameters = true;
        }

        public DataAccess(string connString)
        {
            this._Connection = new SqlConnection();
            this._Connection.ConnectionString = connString;
            this._Command = new SqlCommand();
            this._Parameters = new List<SqlParameter>();
            this._DataTable = new DataTable();
            this._DataAdapter = new SqlDataAdapter();
            this._DataSet = new DataSet();
            this._ClearParameters = true;
        }

        public Boolean TryConnect()
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
            }
            return result;
        }

        public Boolean ExecuteQuery(String commandText)
        {
            return this.ExecuteQuery(commandText, CommandType.Text);
        }

        public Boolean ExecuteQuery(String commandText, CommandType commandType)
        {
            return this.ExecuteQuery(commandText, commandType, this._ClearParameters);
        }

        public Boolean ExecuteQuery(String commandText, CommandType commandType, Boolean clearParameters)
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.CommandText = commandText;
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (SqlParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._Transaction = this._Connection.BeginTransaction(IsolationLevel.Serializable);
                this._Command.Transaction = this._Transaction;
                this._Command.ExecuteNonQuery();
                this._Transaction.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                this._Transaction.Rollback();
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                if (clearParameters == true) this._Parameters.Clear();
            }
            return result;
        }

        public Boolean BatchExecuteQuery(ArrayList commandTexts)
        {
            return this.BatchExecuteQuery(commandTexts, CommandType.Text);
        }

        public Boolean BatchExecuteQuery(ArrayList commandTexts, CommandType commandType)
        {
            return this.BatchExecuteQuery(commandTexts, commandType, this._ClearParameters);
        }

        public Boolean BatchExecuteQuery(ArrayList commandTexts, CommandType commandType, Boolean clearParameters)
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (SqlParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._Transaction = this._Connection.BeginTransaction(IsolationLevel.Serializable);
                this._Command.Transaction = this._Transaction;
                if (commandTexts != null)
                {
                    foreach (String commandText in commandTexts)
                    {
                        this._Command.CommandText = commandText;
                        this._Command.ExecuteNonQuery();
                    }
                }
                this._Transaction.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                this._Transaction.Rollback();
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                if (clearParameters == true) this._Parameters.Clear();
                commandTexts.Clear();
            }
            return result;
        }

        public Boolean ExecuteQueryNoTrans(String commandText)
        {
            return this.ExecuteQueryNoTrans(commandText, CommandType.Text);
        }

        public Boolean ExecuteQueryNoTrans(String commandText, CommandType commandType)
        {
            return this.ExecuteQueryNoTrans(commandText, commandType, this._ClearParameters);
        }

        public Boolean ExecuteQueryNoTrans(String commandText, CommandType commandType, Boolean clearParameters)
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.CommandText = commandText;
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (SqlParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._Command.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                if (clearParameters == true) this._Parameters.Clear();
            }
            return result;
        }

        public Boolean BatchExecuteQueryNoTrans(ArrayList commandTexts)
        {
            return this.BatchExecuteQueryNoTrans(commandTexts, CommandType.Text);
        }

        public Boolean BatchExecuteQueryNoTrans(ArrayList commandTexts, CommandType commandType)
        {
            return this.BatchExecuteQueryNoTrans(commandTexts, commandType, this._ClearParameters);
        }

        public Boolean BatchExecuteQueryNoTrans(ArrayList commandTexts, CommandType commandType, Boolean clearParameters)
        {
            Boolean result = false;
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                if (this._Parameters != null) foreach (SqlParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                if (commandTexts != null)
                {
                    foreach (String commandText in commandTexts)
                    {
                        this._Command.CommandText = commandText;
                        this._Command.ExecuteNonQuery();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                if (clearParameters == true) this._Parameters.Clear();
                commandTexts.Clear();
            }
            return result;
        }

        public DataTable LoadData(String commandText)
        {
            return this.LoadData(commandText, CommandType.Text);
        }

        public DataTable LoadData(String commandText, CommandType commandType)
        {
            return this.LoadData(commandText, commandType, this._ClearParameters);
        }

        public DataTable LoadData(String commandText, CommandType commandType, Boolean clearParameters)
        {
            DataTable result = null;
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                this._Command.CommandText = commandText;
                if (this._Parameters != null) foreach (SqlParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._DataReader = this._Command.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                this._DataTable = new DataTable();
                this._DataTable.Load(this._DataReader);
                result = this._DataTable.Copy();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                if (clearParameters == true) this._Parameters.Clear();
            }
            return result;
        }

        public DataSet LoadMultipleData(String commandText)
        {
            return this.LoadMultipleData(commandText, CommandType.Text);
        }

        public DataSet LoadMultipleData(String commandText, CommandType commandType)
        {
            return this.LoadMultipleData(commandText, commandType, this._ClearParameters);
        }

        public DataSet LoadMultipleData(String commandText, CommandType commandType, Boolean clearParameters)
        {
            DataSet result = new DataSet();
            try
            {
                this._Connection.Open();
                this._Command.Connection = this._Connection;
                this._Command.CommandType = commandType;
                this._Command.CommandText = commandText;
                if (this._Parameters != null) foreach (SqlParameter parameter in this._Parameters) this._Command.Parameters.Add(parameter);
                this._DataAdapter.SelectCommand = this._Command;
                this._DataAdapter.Fill(this._DataSet);
                result = this._DataSet.Copy();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            finally
            {
                this._Connection.Close();
                if (clearParameters == true) this._Parameters.Clear();
            }
            return result;
        }

        #endregion
    }
}
