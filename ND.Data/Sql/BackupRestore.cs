﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Data;
using System.Web;

namespace ND.Data.SQL
{
    public class BackupRestore
    {
        #region Fields

        private String _BackupPath;
        private String _FileName;
        private DataAccess _DataAccess;
        private String _BackupName;

        #endregion

        #region Properties

        public String BackupName { get { return _BackupName; } set { _BackupName = value; } }
        public String FileName { get { return _FileName; } set { _FileName = value; } }

        #endregion        

        #region Methods

        public BackupRestore()
        {
            this._DataAccess = new ND.Data.SQL.DataAccess();
            if (ConfigurationManager.AppSettings["SET:BACKUPPATH"] != null)
            {
                this._BackupPath = ConfigurationManager.AppSettings["SET:BACKUPPATH"].ToString();
                if (this._BackupPath.Contains(":") == false) this._BackupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + this._BackupPath;
            }
            else
            {
                if (HttpContext.Current != null)
                {
                    this._BackupPath = Path.GetDirectoryName(HttpContext.Current.Request.MapPath("/")) + "\\";
                }
                else
                {
                    this._BackupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\";
                }                
            }
        }

        public Boolean Backup()
        {
            return this.Backup(this._FileName);
        }

        public Boolean Backup(String backupFileName)
        {
            Boolean result = false;
            try
            {
                if (backupFileName.Trim() != String.Empty)
                {
                    if (!Directory.Exists(this._BackupPath)) Directory.CreateDirectory(this._BackupPath);
                    backupFileName = this._BackupPath + backupFileName;
                    String database = this._DataAccess.Connection.Database;
                    if (this._BackupName.Trim() == String.Empty) this._BackupName = "Backup";
                    String sql = "USE master; BACKUP DATABASE " + database + " TO  DISK = N'" + backupFileName + "' WITH NOFORMAT, INIT,  NAME = N'" + this._BackupName + "', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";
                    result = this._DataAccess.ExecuteQueryNoTrans(sql, System.Data.CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean Restore()
        {
            return Restore(this._FileName);
        }

        public Boolean Restore(String restoreFileName)
        {
            Boolean result = false;
            try
            {
                if (restoreFileName.Trim() != String.Empty)
                {
                    restoreFileName = this._BackupPath + restoreFileName;
                    String database = this._DataAccess.Connection.Database;
                    String sql = "USE master; RESTORE DATABASE " + database + " FROM  DISK = N'" + restoreFileName + "' WITH  FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 10";
                    result = this._DataAccess.ExecuteQueryNoTrans(sql, System.Data.CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean Delete(String deleteFileName)
        {
            Boolean result = false;
            try
            {
                if (deleteFileName.Trim() != String.Empty)                
                {
                    deleteFileName = this._BackupPath + deleteFileName;
                    File.Delete(deleteFileName);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public DataTable LoadList()
        {
            DataTable result = null;
            try
            {
                if (this._BackupPath.Trim() != String.Empty)
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(this._BackupPath);
                    FileInfo[] fileInfos = directoryInfo.GetFiles("*.bak");
                    if (fileInfos != null)
                    {
                        DataTable dataTable = new DataTable();
                        dataTable.Columns.Clear();
                        dataTable.Columns.Add("FileName", Type.GetType("System.String"));
                        dataTable.Columns.Add("DateTime", Type.GetType("System.String"));
                        foreach (FileInfo fileInfo in fileInfos)
                        {
                            DataRow dataRow = dataTable.NewRow();
                            dataRow["FileName"] = fileInfo.Name;
                            dataRow["DateTime"] = fileInfo.CreationTime.ToString("dd/MM/yyyy hh:mm:ss tt");
                            dataTable.Rows.Add(dataRow);
                            dataTable.AcceptChanges();
                        }
                        result = dataTable.Copy();
                    }                    
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        #endregion
    }
}
