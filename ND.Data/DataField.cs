﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ND.Data
{
    public class DataField
    {
        #region Fields

        private String _Field;
        private DataObject.DataType _DataType;
        private Object _Value;
        private DataObject.ValidateType _ValidateType;

        #endregion

        #region Properties

        public String Field { get { return this._Field; } set { this._Field = value; } }
        public DataObject.DataType DataType { get { return this._DataType; } set { this._DataType = value; } }
        public Object Value { get { return this._Value; } set { this._Value = value; } }
        public DataObject.ValidateType ValidateType { get { return this._ValidateType; } set { this._ValidateType = value; } }

        #endregion    
    
        #region Methods

        public DataField()
        {
            this._Field = String.Empty;
            this._DataType = DataObject.DataType.String;
            this._Value = String.Empty;
            this._ValidateType = DataObject.ValidateType.None;
        }

        #endregion
    }

    public class ConditionField
    {
        #region Fields

        private String _Field;
        private DataObject.DataType _DataType;
        private Object _Value;
        private DataObject.ConditionType _ConditionType;

        #endregion

        #region Properties

        public String Field { get { return this._Field; } set { this._Field = value; } }
        public DataObject.DataType DataType { get { return this._DataType; } set { this._DataType = value; } }
        public Object Value { get { return this._Value; } set { this._Value = value; } }
        public DataObject.ConditionType ConditionType { get { return this._ConditionType; } set { this._ConditionType = value; } }

        #endregion 
       
        #region Methods

        public ConditionField()
        {
            this._Field = String.Empty;
            this._DataType = DataObject.DataType.String;
            this._Value = String.Empty;
            this._ConditionType = DataObject.ConditionType.Equal;
        }

        #endregion
    }

    public class OrderField
    {
        #region Fields

        private String _Field;
        private DataObject.SortType _SortType;

        #endregion

        #region Properties

        public String Field { get { return this._Field; } set { this._Field = value; } }
        public DataObject.SortType SortType { get { return this._SortType; } set { this._SortType = value; } }

        #endregion

        #region Methods

        public OrderField()
        {
            this._Field = String.Empty;
            this._SortType = DataObject.SortType.None;
        }

        #endregion
    }
}
