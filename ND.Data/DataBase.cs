﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ND.Data
{
    public class DataBase
    {
        public static T LoadEntity<T>(DataRow datarow, string[,] mapping) where T : new()
        {
            T entity = new T();
            Type entityType = entity.GetType();
            for (int count = 0; count <= mapping.GetUpperBound(0); count++)
            {
                string propertyName = (string)mapping[count, 0];
                string columnName = (string)mapping[count, 1];

                PropertyInfo property = entityType.GetProperty(propertyName);
                object dbValue = datarow[columnName];

                property.SetValue(entity, dbValue);
            }
            return entity;
        }
    }
}
