﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ND.Data
{
    public class DataObject
    {
        #region Fields

        private String _TableName;
        private List<DataField> _Fields;
        private List<ConditionField> _Conditions;
        private List<OrderField> _Orders;

        #endregion

        #region Properties

        public String TableName { get { return this._TableName; } set { this._TableName = value; } }
        public List<DataField> Fields { get { return this._Fields; } set { this._Fields = value; } }
        public List<ConditionField> Conditions { get { return this._Conditions; } set { this._Conditions = value; } }
        public List<OrderField> Orders { get { return this._Orders; } set { this._Orders = value; } }

        #endregion        

        public enum SQLType
        {
            Insert = 1,
            Update = 2,
            Select = 3,
            Delete = 4
        }

        public enum DataType
        {
            String = 1, //varchar
            NString = 2, //nvarchar
            DateTime = 3, //datetime
            Numeric = 4, //int, decimal, float
            Field = 5, //Field, not using ''
        }

        public enum ValidateType
        {
            Empty = 1, // create '' for empty value (for insert n update)
            None = 2, // ignore empty value (for insert n update)
        }

        public enum ConditionType
        {
            Equal = 1, // use for condition =
            Like = 2, // use for condition like
            NotEqual = 3, // use for condition <>
            Larger = 4, // use for condition >
            Lesser = 5 // use for condition <
        }

        public enum SortType
        {
            None = 0,
            Ascending = 1,
            Descending = 2
        }

        public DataObject()
        {
            this._Fields = new List<DataField>();
            this._Conditions = new List<ConditionField>();
            this._Orders = new List<OrderField>();
        }

        public Boolean ClearAll()
        {
            Boolean result = false;
            try
            {
                this._TableName = String.Empty;
                this.ClearFields();
                this.ClearConditions();
                this.ClearOrders();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean ClearFields()
        {
            Boolean result = false;
            try
            {
                this._Fields.Clear();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean ClearConditions()
        {
            Boolean result = false;
            try
            {
                this._Conditions.Clear();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean ClearOrders()
        {
            Boolean result = false;
            try
            {
                this._Orders.Clear();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddField(String field, Object value, DataType dataType, ValidateType validateType)
        {
            Boolean result = false;
            try
            {
                if (field != String.Empty)
                {
                    DataField dataField = new DataField();
                    dataField.Field = field;
                    dataField.Value = value;
                    dataField.DataType = dataType;
                    dataField.ValidateType = validateType;
                    this._Fields.Add(dataField);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddCondition(String field, Object value, DataType dataType, ConditionType conditionType)
        {
            Boolean result = false;
            try
            {
                if (field != String.Empty)
                {
                    ConditionField conditionField = new ConditionField();
                    conditionField.Field = field;
                    conditionField.Value = value;
                    conditionField.DataType = dataType;
                    conditionField.ConditionType = conditionType;
                    this._Conditions.Add(conditionField);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean AddOrder(String field, SortType sortType)
        {
            Boolean result = false;
            try
            {
                if (field != String.Empty)
                {
                    OrderField orderField = new OrderField();
                    orderField.Field = field;
                    orderField.SortType = sortType;
                    this._Orders.Add(orderField);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public String GenerateSQL(SQLType sqlType)
        {
            return this.GenerateSQL(sqlType, String.Empty, true);
        }

        public String GenerateSQL(SQLType sqlType, Boolean autoClear)
        {
            return this.GenerateSQL(sqlType, String.Empty, autoClear);
        }

        public String GenerateSQL(SQLType sqlType, String condition)
        {
            return this.GenerateSQL(sqlType, condition, true);
        }

        public String GenerateSQL(SQLType sqlType, String staticCondition, Boolean autoClear)
        {
            String result = String.Empty;
            try
            {
                if (this._TableName == String.Empty)
                {
                    ND.Log.LogWriter.WriteLog("TableName is Empty.");
                }
                else
                {
                    switch (sqlType)
                    {
                        case SQLType.Insert:
                        {
                            if ((this._Fields != null) && (this._Fields.Count > 0))
                            {
                                String fieldName = String.Empty;
                                String valueData = String.Empty;
                                if (result == String.Empty) result = "INSERT INTO " + this._TableName;
                                foreach (DataField field in this._Fields)
                                {
                                    if (((field.ValidateType == ValidateType.None) && (this.ParseValue(field.DataType, field.Value, field.ValidateType, true).Trim() != "''") && (this.ParseValue(field.DataType, field.Value, field.ValidateType, true).Trim() != "N''")) || (field.ValidateType == ValidateType.Empty))
                                    {
                                        if (fieldName != String.Empty) fieldName += ", ";
                                        fieldName += field.Field;
                                        if (valueData != String.Empty) valueData += ", ";
                                        valueData += this.ParseValue(field.DataType, field.Value, field.ValidateType, true).Trim();
                                    }
                                }
                                result +=  " ( " + fieldName + " ) VALUES ( " + valueData + " ) ";
                            }
                            break;
                        }
                        case SQLType.Update:
                        {
                            if ((this._Fields != null) && (this._Fields.Count > 0))
                            {
                                String statement = String.Empty;
                                String clause = String.Empty;
                                if (result == String.Empty) result = "UPDATE " + this._TableName + " SET ";
                                foreach (DataField field in this._Fields)
                                {
                                    if (((field.ValidateType == ValidateType.None) && (this.ParseValue(field.DataType, field.Value, field.ValidateType, true).Trim() != "''") && (this.ParseValue(field.DataType, field.Value, field.ValidateType, true).Trim() != "N''")) || (field.ValidateType == ValidateType.Empty))
                                    {
                                        if (statement != String.Empty) statement += ", ";
                                        statement += field.Field + "=" + this.ParseValue(field.DataType, field.Value, field.ValidateType, true).Trim();
                                    }
                                }
                                foreach (ConditionField condition in this._Conditions)
                                {
                                    if (condition.ConditionType == ConditionType.Equal)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + "=" + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Like)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " LIKE " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.NotEqual)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " <> " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Larger)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " > " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Lesser)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " < " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                }
                                result += statement;
                                if ((clause != String.Empty) || (staticCondition != String.Empty))
                                {
                                    result += " WHERE ";
                                    if (clause != String.Empty) result += clause;
                                    if (staticCondition != String.Empty)
                                    {
                                        if (clause != String.Empty)
                                        {
                                            result += " AND " + staticCondition;
                                        }
                                        else
                                        {
                                            result += staticCondition;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case SQLType.Select:
                        {
                            if ((this._Fields != null) && (this._Fields.Count > 0))
                            {
                                String statement = String.Empty;
                                String clause = String.Empty;
                                String sort = String.Empty;
                                foreach (DataField field in this._Fields)
                                {
                                    if (statement != String.Empty) statement += ", ";
                                    statement += field.Field;
                                }
                                foreach (ConditionField condition in this.Conditions)
                                {
                                    if (condition.ConditionType == ConditionType.Equal)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + "=" + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Like)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " LIKE " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.NotEqual)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " <> " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Larger)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " > " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Lesser)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " < " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                }
                                foreach (OrderField order in this._Orders)
                                {
                                    if (sort != String.Empty) sort += ", ";
                                    sort += order.Field;
                                    if (order.SortType == SortType.Descending) sort += " DESC";
                                }
                                result = "SELECT " + statement + " FROM " + this._TableName;
                                if ((clause != String.Empty) || (staticCondition != String.Empty))
                                {
                                    result += " WHERE ";
                                    if (clause != String.Empty) result += clause;
                                    if (staticCondition != String.Empty)
                                    {
                                        if (clause != String.Empty)
                                        {
                                            result += " AND " + staticCondition;
                                        }
                                        else
                                        {
                                            result += staticCondition;
                                        }
                                    }
                                }
                                if (sort != String.Empty) result += " ORDER BY " + sort;
                            }
                            break;
                        }
                        case SQLType.Delete:
                        {
                            if ((this._Fields != null) && (this._Fields.Count > 0))
                            {
                                String clause = String.Empty;
                                if (result == String.Empty) result = "DELETE FROM " + this._TableName;
                                foreach (ConditionField condition in this._Conditions)
                                {
                                    if (condition.ConditionType == ConditionType.Equal)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + "=" + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Like)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " LIKE " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.NotEqual)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " <> " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Larger)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " > " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                    else if (condition.ConditionType == ConditionType.Lesser)
                                    {
                                        if (clause != String.Empty) clause += " AND ";
                                        clause += condition.Field + " < " + this.ParseValue(condition.DataType, condition.Value, true).Trim();
                                    }
                                }
                                if ((clause != String.Empty) || (staticCondition != String.Empty))
                                {
                                    result += " WHERE ";
                                    if (clause != String.Empty) result += clause;
                                    if (staticCondition != String.Empty)
                                    {
                                        if (clause != String.Empty)
                                        {
                                            result += " AND " + staticCondition;
                                        }
                                        else
                                        {
                                            result += staticCondition;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
                if (autoClear == true) ClearAll();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public String ParseValue(DataType dataType, Object value, Boolean checking)
        {
            return this.ParseValue(dataType, value, ValidateType.None, checking);
        }

        public String ParseValue(DataType dataType, Object value, ValidateType validateType, Boolean checking)
        {
            String str = String.Empty;
            try
            {
                if (value != null)
                {
                    switch (dataType)
                    {
                        case DataType.String:
                        case DataType.NString:
                        {
                            if (value == null) str = String.Empty;
                            str = Convert.ToString(value).Trim();
                            str = "'" + ParseCleanValue(str, checking) + "'";
                            if (dataType == DataType.NString) str = "N" + str;
                            break;
                        }
                        case DataType.Numeric:
                        {
                            if (value.GetType() == typeof(System.Boolean)) value = Convert.ToInt32(value);
                            Decimal num = 0;
                            if (Decimal.TryParse(Convert.ToString(value), out num) == true)
                            {
                                str = num.ToString();
                            }
                            else
                            {
                                if (validateType == ValidateType.None)
                                {
                                    str = "''";
                                }
                                else
                                {
                                    str = "0";
                                }
                            }
                            break;
                        }
                        case DataType.DateTime:
                        {
                            DateTime date = DateTime.Parse("1900-01-01 00:00:00");
                            if (DateTime.TryParse(Convert.ToString(value), out date) == true)
                            {
                                int dayI = date.Day;
                                String dayS = dayI.ToString();
                                int monthI = date.Month;
                                String monthS = monthI.ToString();
                                int yearI = date.Year;
                                String yearS = yearI.ToString();
                                int hourI = date.Hour;
                                String hourS = hourI.ToString();
                                int minuteI = date.Minute;
                                String minuteS = minuteI.ToString();
                                int secondI = date.Second;
                                String secondS = secondI.ToString();
                                if (dayS.Length == 1) dayS = String.Concat("0", dayS);
                                if (monthS.Length == 1) monthS = String.Concat("0", monthS);
                                if (hourS.Length == 1) hourS = String.Concat("0", hourS);
                                if (minuteS.Length == 1) minuteS = String.Concat("0", minuteS);
                                if (secondS.Length == 1) secondS = String.Concat("0", secondS);
                                String[] strDateTime = new String[11];
                                strDateTime[0] = yearS;
                                strDateTime[1] = "-";
                                strDateTime[2] = monthS;
                                strDateTime[3] = "-";
                                strDateTime[4] = dayS;
                                strDateTime[5] = " ";
                                strDateTime[6] = hourS;
                                strDateTime[7] = ":";
                                strDateTime[8] = minuteS;
                                strDateTime[9] = ":";
                                strDateTime[10] = secondS;
                                str = String.Concat(strDateTime);
                            }
                            else
                            {
                                if (validateType == ValidateType.None)
                                {
                                    str = String.Empty;
                                }
                                else
                                {
                                    str = "1900-01-01 00:00:00";
                                }
                            }
                            str = "'" + str + "'";
                            break;
                        }
                        case DataType.Field:
                        {
                            str = Convert.ToString(value).Trim();
                            break;
                        }
                    }
                }
                else
                {
                    str = "''";
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return str;
        }

        private String ParseCleanValue(String value, Boolean checking)
        {
            String result = String.Empty;
            int length = value.Length;
            if (length > 0)
            {
                value = value.Replace("'", "''");
                value = value.Replace("--", string.Empty);
                value = value.Replace(";", string.Empty);
                if (length > 8)
                {
                    value = value.Replace("xp_", string.Empty);
                    if (checking == true)
                    {
                        value = value.Replace("delete from", string.Empty);
                        value = value.Replace("drop table", string.Empty);
                    }
                }
                result = value;
            }
            return result;
        }

        public static Boolean IsNumeric(String value)
        {
            Boolean result = false;
            try 
            {
                long.Parse(value); 
                result = true; 
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            try {
                ulong.Parse(value); 
                result = true; 
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            try 
            { 
                float.Parse(value);
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            try 
            { 
                double.Parse(value); 
                result = true; 
            }
            catch (Exception ex) 
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            try 
            {
                decimal.Parse(value); 
                result = true; 
            }
            catch (Exception ex) 
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public static Boolean IsDateTime(String value)
        {
            Boolean result = false;
            try 
            { 
                DateTime.Parse(value); 
                result = true; 
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public static Boolean AddEmptyDataRow(ref DataTable dataTable)
        {
            Boolean result = false;
            try
            {
                DataRow dataRow = dataTable.NewRow();
                int columnCount = 0;
                for (columnCount = 0; columnCount < dataTable.Columns.Count; columnCount++)
                {
                    if (dataTable.Columns[columnCount].AllowDBNull)
                    {
                        dataRow[columnCount] = DBNull.Value;
                    }
                    else
                    {
                        if (dataTable.Columns[columnCount].DataType == typeof(System.Boolean))
                        {
                            dataRow[columnCount] = false;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Byte))
                        {
                            dataRow[columnCount] = 0x00;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Byte[]))
                        {
                            dataRow[columnCount] = new Byte[] { 0x00 };
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Char))
                        {
                            dataRow[columnCount] = ' ';
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.DateTime))
                        {
                            dataRow[columnCount] = DateTime.MinValue;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Decimal))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Double))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Int16))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Int32))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Int64))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.SByte))
                        {
                            dataRow[columnCount] = 0x00;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Single))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.String))
                        {
                            dataRow[columnCount] = String.Empty;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.TimeSpan))
                        {
                            dataRow[columnCount] = TimeSpan.MinValue;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.UInt16))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.UInt32))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.UInt64))
                        {
                            dataRow[columnCount] = 0;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Guid))
                        {
                            dataRow[columnCount] = Guid.Empty;
                        }
                        else if (dataTable.Columns[columnCount].DataType == typeof(System.Object))
                        {
                            dataRow[columnCount] = String.Empty;
                        }
                    }
                }
                dataTable.Rows.InsertAt(dataRow, 0);
                dataTable.AcceptChanges();
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }
    }
}
