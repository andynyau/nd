﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Configuration;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;

namespace ND.Reporting
{
    public class Crystal
    {
        #region Fields

        private String _ReportFile;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer _CrystalReportViewer;
        private String _ReportFormula;
        private Hashtable _HastTable;

        #endregion

        #region Properties

        public String ReportFile { get { return this._ReportFile; } set { this._ReportFile = value; } }
        public CrystalDecisions.Windows.Forms.CrystalReportViewer CrystalReportViewer { get { return this._CrystalReportViewer; } set { this._CrystalReportViewer = value; } }
        public String ReportFormula { get { return this._ReportFormula; } set { this._ReportFormula = value; } }
        public Hashtable HastTable { get { return this._HastTable; } set { this._HastTable = value; } }

        #endregion

        #region Methods

        public Crystal()
        {
            this._HastTable = new Hashtable();
        }

        public Boolean CheckParameterFieldExist(String parameterField)
        {
            Boolean result = false;
            try
            {
                if ((this._ReportFile != String.Empty) && (this._CrystalReportViewer != null))
                {
                    ReportDocument reportDocument = new ReportDocument();
                    String crPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + ConfigurationManager.AppSettings["CR:PATH"].ToString();
                    reportDocument.Load(crPath + this._ReportFile);                    
                    foreach (ParameterFieldDefinition parameterFieldDefinition in reportDocument.DataDefinition.ParameterFields)
                    {
                        if (parameterFieldDefinition.ParameterFieldName == parameterField) result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean CheckFormulaFieldExist(String formulaField)
        {
            Boolean result = false;
            try
            {
                if ((this._ReportFile != String.Empty) && (this._CrystalReportViewer != null))
                {
                    ReportDocument reportDocument = new ReportDocument();
                    String crPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + ConfigurationManager.AppSettings["CR:PATH"].ToString();
                    reportDocument.Load(crPath + this._ReportFile);
                    foreach (FormulaFieldDefinition formulaFieldDefinition in reportDocument.DataDefinition.FormulaFields)
                    {
                        if (formulaFieldDefinition.FormulaName == formulaField) result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public String GetReportFormula()
        {
            String result = String.Empty;
            try
            {
                if ((this._ReportFile != String.Empty) && (this._CrystalReportViewer != null))
                {
                    ReportDocument reportDocument = new ReportDocument();
                    String crPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + ConfigurationManager.AppSettings["CR:PATH"].ToString();
                    reportDocument.Load(crPath + this._ReportFile);
                    result = reportDocument.DataDefinition.RecordSelectionFormula;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean ShowFormReport()
        {
            Boolean result = false;
            try
            {
                if ((this._ReportFile != String.Empty) && (this._CrystalReportViewer != null))
                {
                    ReportDocument reportDocument = new ReportDocument();
                    TableLogOnInfo crTableLogonInfo = new TableLogOnInfo();
                    ConnectionInfo crConnectionInfo = new ConnectionInfo();
                    Tables crTables;
                    String crPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + ConfigurationManager.AppSettings["CR:PATH"].ToString();
                    reportDocument.Load(crPath + this._ReportFile);
                    crConnectionInfo.ServerName = ConfigurationManager.AppSettings["CR:SERVER"].ToString();
                    crConnectionInfo.DatabaseName = ConfigurationManager.AppSettings["CR:DATABASE"].ToString();
                    crConnectionInfo.UserID = ConfigurationManager.AppSettings["CR:USER"].ToString();
                    crConnectionInfo.Password = ConfigurationManager.AppSettings["CR:PASSWORD"].ToString();
                    crTables = reportDocument.Database.Tables;
                    foreach (Table crTable in crTables)
                    {
                        crTableLogonInfo = crTable.LogOnInfo;
                        crTableLogonInfo.ConnectionInfo = crConnectionInfo;
                        crTable.ApplyLogOnInfo(crTableLogonInfo);
                    }
                    if (this._ReportFormula != String.Empty) reportDocument.DataDefinition.RecordSelectionFormula = this._ReportFormula;
                    foreach (ParameterFieldDefinition parameterFieldDefinition in reportDocument.DataDefinition.ParameterFields)
                    {
                        if (parameterFieldDefinition.IsLinked() == false)
                        {
                            ParameterValues parameterValues = new ParameterValues();
                            ParameterDiscreteValue parameterDiscreteValue = new ParameterDiscreteValue();
                            parameterDiscreteValue.Value = DBNull.Value;
                            parameterValues.Add(parameterDiscreteValue);
                            parameterFieldDefinition.ApplyCurrentValues(parameterValues);
                        }
                    }
                    if (this._HastTable != null)
                    {
                        foreach (DictionaryEntry item in this._HastTable)
                        {
                            ParameterValues parameterValues = new ParameterValues();
                            ParameterDiscreteValue parameterDiscreteValue = new ParameterDiscreteValue();
                            parameterDiscreteValue.Value = item.Value;
                            parameterValues.Add(parameterDiscreteValue);
                            reportDocument.DataDefinition.ParameterFields[item.Key.ToString()].ApplyCurrentValues(parameterValues);
                        }
                    }
                    this._CrystalReportViewer.ReportSource = reportDocument;
                    this._CrystalReportViewer.Refresh();
                    result = true;
                }
                else
                {
                    ND.Log.LogWriter.WriteLog("Report Not Set Properly.");
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean ShowWebReport()
        {
            return true;
        }

        #endregion
    }
}
